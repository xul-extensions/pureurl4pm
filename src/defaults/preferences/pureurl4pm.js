pref("extensions.pure-url@palemoon.request_hook_enabled", true);
pref("extensions.pure-url@palemoon.page_inject_type", "observe");
pref(
	 "extensions.pure-url@palemoon.garbage_fields",
	 "action_object_map,action_ref_map,action_type_map,amp,_branch_match_id,ch@quora.com,cid,correlation_id,deep_link,eid,ei@google.com,fb_action_ids,fb_action_types,fbclid,fbrefresh,fb_ref,fb_source,feature@youtube.com,fref@facebook.com,ga_campaign,ga_content,ga_medium,ga_place,ga_source,ga_term,gclid,gs_l,guccounter,guce_referrer,guce_referrer_sig,hc_location@facebook.com,hc_ref@facebook.com,hilit,igshid,mbid,m@blogspot.com,midToken@linkedin.com,_nc_cat,_openstat,partner,pcampaignid,pfmredir,ref@amazon.in,ref@facebook.com,refId@linkedin.com,ref_@imdb.com,ref_src,sdsrc,sfnsn,share@quora.com,source@medium.com,src@addons.mozilla.org,srid,theater,__tn__,trk,trkEmail@linkedin.com,__twitter_impression,uclick,uclickhash,utm_brand,utm_campaign,utm_content,utm_cta,utm_medium,utm_name,utm_place,utm_reader,utm_social-type,utm_source,utm_term,ved,__xts__,yclid");
pref("extensions.pure-url@palemoon.firstrun", true);
pref("extensions.pure-url@palemoon.logging", false);
pref("extensions.pure-url@palemoon.toolsmenu", false);
pref("extensions.pure-url@palemoon.expandlinks", true);
pref("extensions.pure-url@palemoon.shorteners",
	 "bit.ly,cutt.ly,dlvr.it,f.ls,is.gd,ow.ly,soo.gd,t.co,tinyurl.com,trib.al");
