if("undefined"==typeof (PureURL)) {
	var PureURL = {};
}
Components.utils.import("chrome://pureurl4pm/content/common.jsm",PureURL);
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
const puPrefBranch = PureURL.Common.prefBranch;
PureURL.Options = {
		prefExpand : null,
		prefLinkfix : null,
		prefLogging : null,
		prefIntercept : null,
		prefFields : null,
		prefToolsMenu : null,
		chkLinkFix : null,
		chkConsoleLog : null,
		chkIntercept : null,
		chkExpand : null,
		chkToolsMenu : null,
		rbPageInjectType : null,
		txtAdd : null,
		btnAdd : null,
		btnRemove : null,
		lstGarbageFields : null,
		init : function(){
			PureURL.Options.chkLinkFix=document.getElementById('chk-toggle-linkfix');
			PureURL.Options.chkConsoleLog=document.getElementById('chk-console-log');
			PureURL.Options.chkExpand=document.getElementById('chk-expand-links');
			PureURL.Options.chkIntercept=document.getElementById('chk-intercept-requests');
			PureURL.Options.rbPageInjectType=document.getElementById('page-inject-type');
			PureURL.Options.txtAdd=document.getElementById('pureurl-text-add');
			PureURL.Options.btnAdd=document.getElementById('pureurl-button-add');
			PureURL.Options.btnRemove=document.getElementById('pureurl-button-remove');
			PureURL.Options.lstGarbageFields=document.getElementById('pureurl-garbage-fields');
			PureURL.Options.chkToolsMenu=document.getElementById('chk-tools-menu');
			PureURL.Options.chkToolsMenu.addEventListener('command',PureURL.Options.onToolsMenu);
			PureURL.Options.chkLinkFix.addEventListener('command',PureURL.Options.toggleFixLink);
			PureURL.Options.chkConsoleLog.addEventListener('command',PureURL.Options.toggleLogging);
			PureURL.Options.chkExpand.addEventListener('command',PureURL.Options.toggleExpandLinks);
			PureURL.Options.chkIntercept.addEventListener('command',PureURL.Options.toggleInterceptRequests);
			PureURL.Options.rbPageInjectType.addEventListener('command',PureURL.Options.switchPageInjectType);
			PureURL.Options.txtAdd.addEventListener('input',PureURL.Options.onTextChange);
			PureURL.Options.btnAdd.addEventListener('command',PureURL.Options.onAdd);
			PureURL.Options.btnAdd.setAttribute('disabled','true');
			PureURL.Options.btnRemove.addEventListener('command',PureURL.Options.onRemove);
			PureURL.Options.btnRemove.setAttribute('disabled','true');
			PureURL.Options.lstGarbageFields.addEventListener('select',PureURL.Options.onListSelect);

			PureURL.Options.prefLinkFix = puPrefBranch.getCharPref("page_inject_type","");
			if(PureURL.Options.prefLinkFix.length>0){
				PureURL.Options.chkLinkFix.setAttribute('checked','true');
				document.getElementById("page-inject-type").selectedIndex=(PureURL.Options.prefLinkFix=="onload" ? 1 : 0);
			} else {
				PureURL.Options.chkLinkFix.removeAttribute('checked');
			}
			PureURL.Options.handleRadioButtons(PureURL.Options.prefLinkFix.length > 0);

			PureURL.Options.prefLogging=puPrefBranch.getBoolPref("logging",false);
			if(PureURL.Options.prefLogging){
				PureURL.Options.chkConsoleLog.setAttribute('checked','true');
			}

			PureURL.Options.prefIntercept=puPrefBranch.getBoolPref("request_hook_enabled",true);
			if(PureURL.Options.prefIntercept){
				PureURL.Options.chkIntercept.setAttribute('checked','true');
			}
			
			PureURL.Options.prefExpand=puPrefBranch.getBoolPref("expandlinks",false);
			if(PureURL.Options.prefExpand){
				PureURL.Options.chkExpand.setAttribute('checked','true');
			}

			PureURL.Options.prefFields = puPrefBranch.getCharPref("garbage_fields","");
			let prefFieldsArray = PureURL.Options.prefFields.split(",").sort();
			for(let i=0;i<prefFieldsArray.length;i++){
				PureURL.Options.lstGarbageFields.appendItem(prefFieldsArray[i],i);
			}

			PureURL.Options.prefToolsMenu = puPrefBranch.getBoolPref("toolsmenu",true);
			if(PureURL.Options.prefToolsMenu){
				PureURL.Options.chkToolsMenu.setAttribute('checked','true');
			}

		},
		/**
		 * Toggles disabling radio buttons,<br>
		 * depending on whether fix links on pages is turned off.
		 */     
		handleRadioButtons : function(linkfix){
			if(linkfix==true){
				document.getElementById('rbtn-linkfix-dynamic').removeAttribute('disabled');
				document.getElementById('rbtn-linkfix-static').removeAttribute('disabled');
				PureURL.Options.chkExpand.removeAttribute('disabled');
			} else {
				document.getElementById('rbtn-linkfix-dynamic').setAttribute('disabled','true');
				document.getElementById('rbtn-linkfix-static').setAttribute('disabled','true');
				PureURL.Options.chkExpand.setAttribute('disabled','true');
				PureURL.Options.rbPageInjectType.selectedIndex = 0;
			}
		},
		onOk : function(){
			puPrefBranch.setBoolPref("expandlinks",PureURL.Options.prefExpand);
			puPrefBranch.setBoolPref("request_hook_enabled",PureURL.Options.prefIntercept);
			puPrefBranch.setBoolPref("logging",PureURL.Options.prefLogging);
			puPrefBranch.setBoolPref("toolsmenu",PureURL.Options.prefToolsMenu);
			puPrefBranch.setCharPref("page_inject_type",PureURL.Options.prefLinkFix);
			puPrefBranch.setCharPref("garbage_fields",PureURL.Options.prefFields);
			return true;
		},
		/**
		 * Event handler for expand links checkbox.
		 */
		toggleExpandLinks : function(){
			PureURL.Options.prefExpand = PureURL.Options.chkExpand.hasAttribute('checked');
		},
		/**
		 * Event handler for fix links checkbox.
		 */
		toggleFixLink : function(event){
			let checked = event.target.hasAttribute('checked');
			PureURL.Options.handleRadioButtons(checked);
			// If checked, set state by currently selected radio button, else
			// make it blank.
			PureURL.Options.prefLinkFix = checked ? PureURL.Options.rbPageInjectType.selectedIndex == 0 ? "observe" : "onload" : "";
			PureURL.Options.prefExpand = checked;
			PureURL.Options.chkExpand.checked = checked;
		},
		/**
		 * Event handler for log to console checkbox
		 */
		toggleLogging : function(event){
			PureURL.Options.prefLogging = event.target.hasAttribute('checked');
		},
		/**
		 * Event handler for intercept requests checkbox.
		 */
		toggleInterceptRequests : function(event) {
			PureURL.Options.prefIntercept = event.target.hasAttribute('checked');
		},
		/**
		 * Event handler for page injection type radio buttons.
		 */
		switchPageInjectType : function(){
			PureURL.Options.prefLinkFix = PureURL.Options.rbPageInjectType.selectedIndex == 0 ? "observe" : "onload";
		},
		/**
		 * Event handler for add button.
		 */
		onAdd : function(){
			let present = false;
			PureURL.Options.prefFields = puPrefBranch.getCharPref("garbage_fields","");
			let prefFieldsArray = PureURL.Options.prefFields.split(",").sort();
			let fieldVal = PureURL.Options.txtAdd.value.trim();
			for(let i=0;i<prefFieldsArray.length;i++){
				if(prefFieldsArray[i]== fieldVal){
					present = true;
					break;
				}
			}
			if(!present){
				PureURL.Options.lstGarbageFields.insertItemAt(0,fieldVal,fieldVal);
				PureURL.Options.prefFields += (PureURL.Options.prefFields.length > 0 ?  "," : "") +fieldVal;
			}
			PureURL.Options.prefFields=PureURL.Options.sortFields(PureURL.Options.prefFields);
			PureURL.Options.txtAdd.value="";
			PureURL.Options.onTextChange();
		},
		/**
		 * Sorts garbage fields alphabetically.
		 */
		sortFields(fields){
			return fields.split(",").sort().toString();
		},
		/**
		 * Event handler for remove button.
		 */
		onRemove : function(){
			let count = PureURL.Options.lstGarbageFields.selectedCount;
			let deleteItems=PureURL.Options.lstGarbageFields.selectedItems;
			if(deleteItems.length == 1){
				PureURL.Options.prefFields = PureURL.Options.prefFields.replace(deleteItems[0].label+",","").replace(","+deleteItems[0].label,"").replace(deleteItems[0].label,"");
			} else {
				for(var i=0;i<deleteItems.length;i++){
					PureURL.Options.prefFields=PureURL.Options.prefFields.replace(deleteItems[i].label+",","").replace(","+deleteItems[i].label,"").replace(deleteItems[i].label,"");
				}
			}
			while(count--){
				let item = PureURL.Options.lstGarbageFields.selectedItems[0];
				PureURL.Options.lstGarbageFields.removeItemAt(PureURL.Options.lstGarbageFields.getIndexOfItem(item));
			}
			PureURL.Options.prefFields=PureURL.Options.sortFields(PureURL.Options.prefFields);
		},
		/**
		 * Event handler for list selection.
		 */
		onListSelect : function(){
			let disabledFlag = PureURL.Options.lstGarbageFields.selectedCount == 0 ? 'true' : 'false';
			PureURL.Options.btnRemove.setAttribute('disabled',disabledFlag);
		},
		/**
		 * Event handler for textbox typing.
		 */
		onTextChange : function() {
			let disabledFlag = PureURL.Options.txtAdd.value.trim().length == 0  ? 'true' : 'false';
			PureURL.Options.btnAdd.setAttribute('disabled',disabledFlag);
		},
		/**
		 * Event handler for Tools menu display item toggle.
		 */
		onToolsMenu : function(){
			PureURL.Options.prefToolsMenu=PureURL.Options.chkToolsMenu.hasAttribute('checked');
		},
		cleanUp : function(){
			PureURL.Options.chkExpand.removeEventListener('command',PureURL.Options.toggleExpandLinks);
			PureURL.Options.chkLinkFix.removeEventListener('command',PureURL.Options.toggleFixLink);
			PureURL.Options.chkConsoleLog.removeEventListener('command',PureURL.Options.toggleLogging);
			PureURL.Options.chkIntercept.removeEventListener('command',PureURL.Options.toggleInterceptRequests);
			PureURL.Options.rbPageInjectType.removeEventListener('command',PureURL.Options.switchPageInjectType);
			PureURL.Options.txtAdd.removeEventListener('input',PureURL.Options.onTextChange);
			PureURL.Options.btnAdd.removeEventListener('command',PureURL.Options.onAdd);
			PureURL.Options.btnRemove.removeEventListener('command',PureURL.Options.onRemove);
			PureURL.Options.lstGarbageFields.removeEventListener('select',PureURL.Options.onListSelect);
		}
};
window.addEventListener("load", PureURL.Options.init, false);
window.addEventListener("unload",PureURL.Options.cleanUp,false);