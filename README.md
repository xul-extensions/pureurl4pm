<h2>Pure URL for Pale Moon</h2>

Strips garbage referer tracking fields like 'utm_source' from links, and expands shortened links. The strip feature is forked from the [original Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/pure-url/). Pale Moon users can install the latest version directly from [here](https://addons.palemoon.org/addon/pureurl4pm/), Basilisk users can install it from [here](https://addons.basilisk-browser.org/addon/pureurl4pm/).

By default it removes the most common tracking parameters like `utm_source`, you can customize it by adding your own in the Garbage fields list' in extension settings. This is a comma separated list where each field shown is automatically removed from all URLs. You can include domain specific filters, for example `fbclid@facebook.com` will only remove the parameter `fbclid` from the Facebook domain but allow it elsewhere. 

In addition, it also can expand links from popular URL expansion services and reveal them as a tooltip above the link.
Currently supports the following services -
- [bit.ly](https://bit.ly)
- [f.ls](https://f.ls)
- [is.gd](https://is.gd) 
- [tinyurl.com](https://tinyurl.com)
- [trib.al](https://trib.al)